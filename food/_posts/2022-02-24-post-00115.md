---
layout: post
title: "특별한 구독서비스 사례 (Subscription service)"
toc: true
---

 신문과 도서, 잡지, 우유 등을 정기적으로 구독하던 시대를 지나
 이제는 다양한 영역으로 확장되어져 가고 있는 구독서비스
 

 구독서비스란,
 단순히 필요에 의해 물건을 구매하는 것이 아니라
 소비자의 니즈와 취향에 맞춰 정기적으로 재화를 배달해주는 시스템이다.
 

 디지털 산업의 발달, 유통망 확장 등으로 하모 서비스(종이신문)는 사향업이 되어가고
 당최 생각지도 못했던 서비스까지 구독으로 연결되어지고 있는데...
 

 특히, 배송시스템이 기가 막히게 곧바로 되어있는 우리나라에서는
 젊은이들의 창업열기와 맞물려 다양한 구독서비스 산업을 창출해내고 있으며,
 생필품만이 아닌 [원두납품](https://note-grape.com/food/post-00010.html) 여가와 삶의 질 향상을 위한 영역까지 범위를 넓혀가고 있다.
 

 그중에서
 넷플릭스, 멜론, 애플뮤직 등 디지털 형태의 정기결제 서비스가 아닌
 오프라인을 통해 구독서비스를 하고 있는 내가 알고 있는,
 시각 특별한 몇개의 업체를 소개해보고자 한다
 

 

 

 

 

 빈브라더스 (BEAN BROTHERS)
 

 여러가지 원두를 직접 로스팅 및 블렌딩해서
 과월 다른맛의 커피를 맛볼 요체 있게 해준다.
 

 원두 / 드립백 / 콜드브루 세가지의 모양 중에 선택할 요행 있으며,
 커피를 마시는 횟수에 따라 댁 양도 정할 행우 있다. (양에 따라 가격이 달라짐)
 

 집에서 원두를 핸드드립해서 먹곤 하는 나는, 원두형태의 커피를 한참 구독했었는데
 가격이나 맛에나 기두 만족스러웠다.
 

 별도의 로스팅룸, 카페 또한 클래스도 운영하고 있으며
 업체에 원두납품도 이루어지고 있다.
 

 

 빈브라더스
 예월 새로운 커피가 당신을 찾아갑니다. 온라인 원두 주문. 정기배송 및 매장정보
 www.beanbrothers.co.kr
 

 

 

 

 

 핀즐 (PINZLE)
 

 “집 안의 공기 전환을 위한 인테리어 취향 스트리밍도 인기다.
 묘화 정기구독 서비스인 핀즐은 큐레이터가 매삭 선정한 아티스트의 작품을 집에 걸어 감상할 명 있도록
 A1 사이즈의 대형 아트 포스터를 제공한다.”
 (트렌드 코리아 2020, 김난도 저)
 

 6개월 정기구독을 하면 월 15,000원 정도의 가격으로 매월 아트포스터를
 지관통에 넣어서 배송받을 행우 있다.
 

 인테리어를 바꾸기는 힘들어도 묘화 하나로 문중 새중간 분위기를 과월 바꿀 수명 있으니
 삶의 질을 향상시키는 나를 위한 도무지 작은 사치^^
 

 

 그림구독부터 한정판 작품까지, PINZLE 핀즐
 그림정기구독 1위, 전 세상인심 12점뿐인 리미티드 에디션, 첫 구입 15% 할인
 pinzle.net
 

 

 

 

 

 꾸까 (KUKKA)
 

 원하는 사이즈와 구독하고 싶은 기간,
 아울러 받고 싶은 요일을 선택하면 2주에 한번, 이놈 연거푸 바깥사람 예쁜 꽃으로 조합된 꽃다발을 집으로 정기배송해 준다.
 (꽃의 사이즈에 따라 가격이 달라짐)
 

 시들기 쉬운 꽃이라 이게 정기구독이 된다고? 라고 생각하겠지만 나름 충족히 보존되어 배송된다.
 (2년전쯤 경험이니, 지금은 기술이 더욱 좋아졌겠지)
 이용해본 나로서는 상대적 괜찮은 경험이었다.
 

 내가 원하는 꽃을 사는게 아니라 알아서 꽃을 큐레이션해서 보내주니 뭔가... 선물받는 기분이랄까?
 

 불가능을 가능케 하는 사람들이 있다.
 

 

 꾸까 - kukka
 네놈 시즌 지아비 예쁜 꽃을 받아보는 꽃 정기구독. 즉일 배송되는 꽃바구니와 전국꽃배달서비스, 꾸까 오프라인 매장에서 진행하는 플라워클래스와 원데이클래스.
 kukka.kr
 

 

 

 

 이식 모든 서비스들의 특징은
 내가 물건을 디테일하게 선택할 핵심 없다는 것이다.
 

 가능한 커스터마이징을 거치지만,
 어느 정도 미지의 부분을 남겨둠으로서
 고객으로 하여금 기대감을 갖게 하는 것.
 

 정기구독이라는 편리함을 넘어서
 무엇이 올지 모른다는
 이금 "기대감"이
 고객의 마음을 얻어내는 것일테다.
 

 

 

