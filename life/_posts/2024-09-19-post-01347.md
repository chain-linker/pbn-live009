---
layout: post
title: "2023 - 비트코인의 지위가 강해진 해"
toc: true
---


## 2023 - 비트코인의 지위가 강해진 해
 아래는 2023.1.1. 부터 위인 (2023.3.31.)까지의 비트코인 가격 추이다.
 연초부터 73.86%이나 올랐다.
 그런데도 아직 3643만원 전후... 왕세 전고점까지 가려면 2.2배 정도는 올라 줘야 한다.

 

 지금의 경제 상황은 원체 불안불안 하다. 계속해서 문제가 되는 은행들이 나오고, 이들이 일방 단특 미국 정부의 신속한 대응으로 처리되고 있다.
 금리는 얼추 내려올 기미가 안보이고, 여태껏 시비 상황도 내리 유지되고 있다.
 뿐만 아니라 세계는 '중국의 대만 침공'이라고 하는 초대형 경합 리스크를 다시 품고 불안불안하게 흘러가고 있다.
 

 이러한 상황에도 불구하고, 게다가 이렇다 할 Crash가 오지 않고 있다. 붕괴가 없는 것이다.
 예전과 같은 위기를 기다리는 건 아니지만, 요연히 과거였다면 대신 경제위기를 맞이할 것도 같은데, 요리조리 도로 버티고 있는 것을 알 길운 있다.
 

 나는 '비트코인'을 위시한 가상화폐 시장의 존재가 과거의 경제와는 달리 새로운 변수가 되고 있다고 생각한다.
 예전에는 정통 금융시장이 무너지면 유동성을 구할 곳이 가위 정부가 비발 찍는 거 말고는 없었다.
 그렇지만 [바이비트](https://obesecollect.link/life/post-00119.html) 지금은? 가상화폐시장에 보존되어 있던 정의 - 바로 돈들 -가 시장 붕괴 조짐이 보일 때마다 정통시장으로 흘러 들어와 가격이 떨어진 매물을 받아 주면서 예전과는 다른 양상을 보여 주고 있는 것이다.
 

 더구나 돈을 찍어 서장 인플레를 만들어도 이게 새로 가상화폐 시장으로 흘러 들어가 비렁뱅이 인간쓰레기 같은 알트코인에 흘러 들어가 가치가 증발하며 인플레의 부작용이 완화되기도 하고, 새로이 그쪽 시장 내에서 돌고 돌면서 일종의 돈의 저수지 역할을 하는 것 같기도 하다.
 

 가상화폐, 가상자산의 가치는 정말이지 정통 금융이 흔들리고 기존의 화폐경제가 문제점을 노출할 때마다 노형 존재감을 드러내는 것 같다.
 

 나는 한결같이 연거푸 비트코인을 사멸 모으고 있다. 0.01 BTC 단위로 돈이 생길 때마다 모은다. 어쨌든지 나는 비트코인을 신뢰한다. 반면에 한편으로는, 비토코인 이외의 그쪽 아무개 알트코인에 대해서도 시나브로 신뢰를 거두고 있다. 목하 내게 있어 하여간 고용주 중요한 가상화폐는 비트코인이고, 그대 외의 아무개 알트코인도 (이더리움이나 리플) 천만 설득력을 확보하지 못했다.
 

 비트코인에 대해서는 날로 정형 확신이 강해지고, 저변도 늘어나고 있다고 본다.
 바야흐로 한국에서 생각보다 비트코인을 쓸 목숨 있는 가게도 만만 늘어나고 있다. 라이트닝 기술이란 걸 이용해서 NFC 모바일 솜씨 결제 하듯이 쉽게 결제할 운 있는 수단이 확보되고 있는 것이다.
 비트코인은 아울러 차회 2022년말 - 2023년초의 위기에서 알게 모르게 역할을 하면서 갈수록 존재감을 높여가고 있다.
 2023년 3월, 은행이 망할 마당 비트코인이 올랐다는 것을 잊지 말라.
 

##### '수렵채집일기 > Blockchain 라이브러리' 카테고리의 다른 글

### 태그

### 관련글

### 댓글0
